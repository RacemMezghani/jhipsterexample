package tn.mss.ee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.mss.ee.domain.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {}
