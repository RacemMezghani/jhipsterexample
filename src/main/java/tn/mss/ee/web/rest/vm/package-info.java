/**
 * View Models used by Spring MVC REST controllers.
 */
package tn.mss.ee.web.rest.vm;
